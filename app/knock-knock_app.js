

/*
*
* Copyright (C) 2018 Tamás Lakatos <taml@itu.dk>
* 
* This file is part of "knock-knock_game" and cannot be copied and/or distributed without the express permission of the following 
* persons: Tamás Lakatos <taml@itu.dk>, Sissel Angela Ringvad <sanr@itu.dk> and Sarah Grossi <sagr@itu.dk>.
* 
*/


const socket = io.connect();


/* ----------------- pre-chatroom-join ----------------- */ 


$(function() {

      /* Binds a "focus" handler to the "chatroom-id-digit-input" box. */

      $( '#chatroom-id-digit-input' ).focus();
      
      
      /* Re-binds the "focus" handler to the "chatroom-id-digit-input" box if out of focus. */

      $( '#chatroom-id-digit-input' ).focusout(function() {

            $('#chatroom-id-digit-input').focus();
      });


      /* Emits an event to the server to create a chatroom. */

      $( '#create-chatroom' ).click(function() {

            // Remove "invalid-chatroom-id-digit-input" class, if exists.

            if( $( '#chatroom-id-digit-input' ).hasClass( 'invalid-chatroom-id-digit-input' ) )
                  $( '#chatroom-id-digit-input' ).removeClass( 'invalid-chatroom-id-digit-input' );

            // Remove "invalid-notification" message.

            if ( $( '.invalid-notification' ).length > 0 )
                  $( '.invalid-notification' ).remove();
  
            // Receives a callback from the server with a registered 'chatroomId'.

            socket.emit( 'create chatroom', function(response) {

                  if( response.result ) {
                  
                        // Hide the "create-chatroom" button.

                        $( '#create-chatroom' ).hide();

                        // Append chatroomId to "chatroom-id-digit-input".

                        $( '#chatroom-id-digit-input' ).val(response.chatroomId);
                  }
                  else {

                        // Remove "chatroom-already-in-use-facilitator-text", if exists. 

                        if ( $( '#' + response.facilitator.ID ).length > 0 )
                              $( '#' + response.facilitator.ID ).remove();
                        
                        // Append "chatroom-already-in-use-facilitator-text".

                        $( '#invalid-notification-section' ).append( '<p class="invalid-notification" id="'+ response.facilitator.ID +'">'+ response.facilitator.TEXT +'</p>' );

                        // Remove "chatroom-already-in-use-facilitator-text", after 20 seconds.

                        setTimeout(function() {
          
                              if ( $( '#' + response.facilitator.ID ).length > 0 )
                                    $( '#' + response.facilitator.ID ).remove();

                        }, 20000);
                  }
            });
      });


      /* If the player doesn't join the newly created chatroom within one minute than the chatroom gets deleted. */ 

      socket.on( 'too late to join chatroom', function(response) {

            // Remove "late-to-join-chatroom-facilitator-text", if exists.

            if ( $( '#' + response.facilitator.ID ).length > 0 )             
                  $( '#' + response.facilitator.ID ).remove();

            // Remove "invalid-notification" message.

            if ( $( '.invalid-notification' ).length > 0 )
                  $( '.invalid-notification' ).remove();

            // Append "late-to-join-chatroom-facilitator-text".

            $( '#invalid-notification-section' ).append( '<p class="invalid-notification" id="'+ response.facilitator.ID +'">' + response.facilitator.TEXT +'</p>' );

            // Show the "create-chatroom" button and reset "chatroom-id-digit-input" value.

            $( '#create-chatroom' ).show();
            $( '#chatroom-id-digit-input' ).val("");

            // Remove "late-to-join-chatroom-facilitator-text", after 20 seconds. 

            setTimeout(function() {
          
                  if ( $( '#' + response.facilitator.ID ).length > 0 )
                        $( '#' + response.facilitator.ID ).remove();

            }, 20000);
      });


      /* Emit an event to the server to join a chatroom. */
         
      $( '#join-chatroom' ).click(function() {

            // Get chatroomId from "chatroom-id-digit-input".

            var chatroomId = $( '#chatroom-id-digit-input' ).val().toUpperCase();          

            // Emits an event to the server to validate whether the chatroomId is valid or not.

            socket.emit( 'validate chatroom id', chatroomId, function(response) {

                  if( response.result ) {

                        // Receives a callback from the server that "you can join" the chatroom.

                        socket.emit( 'join chatroom', chatroomId, function(response) {

                              // Load the "post-chatroom-messaging.html" page with ajax. - callback function on ajaxComplete jquery built in fucntion.

                              $( 'body' ).load( 'post-chatroom-messaging.html', function() {

                                    // Append player and other players color in the chatroom. 

                                    appendPlayerColor(response.chatroomId, response.player, response.otherPlayers);

                                    // Append chat messages to "chatroom-messages".

                                    appendPreviousChatMessages(response.messages);
                              });
                        });
                  }
                  else {

                        // Remove "invalid-chatroom-id-facilitator-text", "chatroom-is-full-facilitator-text", "game-already-started-facilitator-text", "no-owner-joined-chatroom-facilitator-text", if exists.

                        if ($( '#' + response.facilitator.ID ).length > 0)                              
                              $( '#' + response.facilitator.ID ).remove();

                        // Append "invalid-chatroom-id-facilitator-text", "chatroom-is-full-facilitator-text", "game-already-started-facilitator-text", "no-owner-joined-chatroom-facilitator-text", if exists. 

                        $( '#invalid-notification-section' ).append( '<p class="invalid-notification" id="'+ response.facilitator.ID +'">'+ response.facilitator.TEXT +'</p>' );

                        // Add "invalid-chatroom-id-digit-input".

                        $( '#chatroom-id-digit-input' ).addClass( 'invalid-chatroom-id-digit-input' );
                        
                        // Remove "invalid-chatroom-id-facilitator-text", "chatroom-is-full-facilitator-text", "game-already-started-facilitator-text", "no-owner-joined-chatroom-facilitator-text", if exists.
                        
                        setTimeout(function(){
          
                              if ( $( '#' + response.facilitator.ID ).length > 0 )
                                    $( '#' + response.facilitator.ID ).remove();

                        }, 20000);
                  }  
            });
      });


      /* Remove the "invalid-chatroom-id-digit-input" after player starts typing. */ 

      $( '#chatroom-id-digit-input' ).keydown(function() {
            
            $( '#chatroom-id-digit-input' ).removeClass( 'invalid-chatroom-id-digit-input' );

            // Remove "invalid-notification" message.

            if ($( '.invalid-notification' ).length > 0)
                  $( '.invalid-notification' ).remove();
      });


      /* Append player and other players color in the chatroom. */

      function appendPlayerColor(chatroomId, player, otherPlayers) {

            // Append "chatroom-id-knock-knock-logo" for players to see in which chatroom they are. 
            
            $( '#post-chatroom-messaging-knock-knock-logo' ).after( '<p id="chatroom-id-knock-knock-logo">' + chatroomId + '</p>' );

            // Append the player color.

            $( '#assigned-character-color' ).append( '<div class="character" id="'+ player.socketId +'"></div>' );
            $( '#' + player.socketId ).css( 'border-color', player.color );
            $( '#' + player.socketId ).css( 'background-color', player.color );

            // If the player is the chatroom owner, than append "start-session-button" else append "ready-to-play-button". 

            if( player.chatroomOwner )
                  $( '#ready-start-buttons' ).append( $( '<button id="start-session-button"> START SESSION </button>' ).on('click', startSessionButton ));
            else 
                  $( '#ready-start-buttons' ).append( $( '<button id="ready-to-play-button"> READY </button>' ).on('click', readyToPlayButton ));

            // Foreach other player append "player-color".

            otherPlayers.forEach(function(player) {

                  $( '#other-players-color-group' ).append( '<div class="player-color" id="'+ player.socketId +'"> </div>' );
                  $( '#' + player.socketId ).css( 'background-color', player.color );
                  $( '#' + player.socketId ).css( 'border-color', player.color );

                  if(player.readyToPlay)
                        $( '#'+ player.socketId ).append( '<span class="ready"> R </span>' );
            });
      }

      
      /* This function appends all the previous messages that have been published before the player has joined the chatroom. */

      function appendPreviousChatMessages(messages) {

            // Append all previous chat messages to "chatroom-messages".

            messages.forEach(function(message) {
                  
                  $('#chatroom-messages').append('<div id="'+ message.type +'" style="background-color:'+ message.color +'">'+ message.message +'</div>');
                  $('#chatroom-messages').scrollTop($('#chatroom-messages')[0].scrollHeight);
            });
      }

      
      /* The owner of the chatroom starts the game session. */

      function startSessionButton() {

            // Disable "start-session-button" button.

            $( '#start-session-button' ).prop('disabled', true);

            // The session can only start, if there are at least three players ready.

            socket.emit('check number of players', function(response) {

                  if(response.result) {

                        // Remove "invalid-notification" message and "start-session-button".

                        if ($( '.invalid-notification' ).length > 0)
                              $( '.invalid-notification' ).remove();  

                        $( '#start-session-button' ).remove();

                        // Emit an event to the server to start a session. 

                        setTimeout(function() {

                              socket.emit('start session');
                        
                        }, 1000);
                  }
                  else {

                        // Remove "not-enought-players-facilitator-text" if exists.

                        if ( $( '#' + response.facilitator.ID ).length > 0 )                              
                              $( '#' + response.facilitator.ID ).remove();

                        // Append "not-enought-players-facilitator-text"/

                        $( '#chatroom-bottom-buttons-and-questions' ).after('<p class="invalid-notification" id="'+ response.facilitator.ID +'">'+ response.facilitator.TEXT +'</p>');

                        // Remove "not-enought-players-facilitator-text", after 20 seconds.

                        setTimeout(function() {
                      
                              if ( $( '#' + response.facilitator.ID ).length > 0 )                              
                                    $( '#' + response.facilitator.ID ).remove();
                        
                        }, 20000);

                        // Enable "start-session-button" button.

                        $( '#start-session-button' ).prop('disabled', false);
                  }
            });
      }


      /* Sends a message to other players that the player is ready to play. */ 

      function readyToPlayButton() {

            // Disable "ready-to-play-button" button.

            $( '#ready-to-play-button' ).prop('disabled', true);

            // Emit an event to the server that the player is ready to play. 

            socket.emit( 'ready to play', function() {

                  // Disable "ready-to-play-button" button.
                              
                  $( '#ready-to-play-button' ).remove();
            });
      }

      /* Fires when the client gets disconnected from the server.*/

      socket.on('disconnect', function() {

            // Remove "facilitator-notification" or "invalid-notification", if exists.

            if ($( '.facilitator-notification' ).length > 0)
                  $( '.facilitator-notification' ).remove();
            
            if ($( '.invalid-notification' ).length > 0)
                  $( '.invalid-notification' ).remove();

            // Append "server-disconnect" notification to the player.

            if ($( '#facilitator-notification-section' ).length > 0)
                  $( '#facilitator-notification-section' ).append( '<p class="facilitator-notification" id="server-disconnect"> You have been disconnected from the server! </p>' );

            if ($( '#invalid-notification-section' ).length > 0)
                  $( '#invalid-notification-section' ).append( '<p class="invalid-notification" id="server-disconnect"> You have been disconnected from the server! </p>' );  
            
            if($( '#guess-who-is-who-knock-knock-logo' ).length > 0)
                  $( '#guess-who-is-who-knock-knock-logo' ).after( '<p class="facilitator-notification" id="server-disconnect"> You have been disconnected from the server! </p>' );
      });
});


/* ----------------- pre-chatroom-join ----------------- */


$(document).ajaxComplete(function() {

   
      /* ----------------- post-chatroom-messaging ----------------- */


      $(function() {

            
            /* This function creates awerness for all the players in the chatroom, that a new player has joined the chatroom. */

            socket.on( 'broadcast player joined chatroom', function(player) {

                  $("#other-players-color-group").append('<div class="player-color" id="'+ player.socketId +'"> </div>');
                  $( '#' + player.socketId).css('background-color', player.color);
                  $( '#' + player.socketId).css('border-color', player.color);
            });

            
            /* This function appends a ready signal, for all the players to know that a player is ready to play. */

            socket.on( 'awareness on player ready', function(socketId) {
                    
                  $( '#' + socketId ).append( '<span class="ready"> R </span>' );
            });

            
            /* This function redirects the player from the chatroom to the "post-chatroom-messaging.html" page. */

            socket.on( 'remove player from chatroom', function(){

                  location.reload(true);
            });


            /* This function sends feedback to all the players, that a player has left the chatroom. */

            socket.on( 'remove player from chatroom feedback', function(socketId) {

                  if( $( '#' + socketId ).length > 0 )
                        $( '#' + socketId ).remove();
            });


            /* Append "facilitator-overlay" with "roud-started-facilitator-text". */
            
            socket.on('facilitator-round-started', function(response) {
                  
                  // Remove ".facilitator-notification" message.
                                                      
                  if ($( '.facilitator-notification' ).length > 0)
                        $( '.facilitator-notification' ).remove();

                  // Remove any "facilitator-overlay", if exists.

                  if ( $( '#facilitator-overlay' ).length > 0 )                              
                        $( '#facilitator-overlay' ).remove();

                  // Remove "points-dashboard", if exists.

                  if( $( '#player-points-dashboard' ).length > 0 )
                        $( '#player-points-dashboard' ).remove();

                  // Show the post-chatroom-messaging.html if not visible.

                  if( !$('.container').is(":visible") ) 
                        $( '.container' ).show();

                  // Append "roud-started-facilitator-text".
                  
                  $( 'body' ).append( '<div id="facilitator-overlay"> <div class="facilitator-text" id="'+ response.facilitator.ID +'">' + response.facilitator.TEXT +'</div></div>');
            });

            
            /* This function assignes a character with three personality traits to help the player to better role play their character. */

            socket.on( 'assign character to player', function(response) {

                  // Add the character photograph.

                  $( '#' + response.player.socketId ).css('content', 'url('+ response.player.currentCharacter.URL +')');
                  $('.character').css('height', 'auto');

                  // Removes "personality-trait" if exists.

                  if ( $( '.personality-trait' ).length > 0 )
                        $( '.personality-trait' ).remove();

                  // Append "personality-trait" to character.             

                  $( '#character-personality-traits' ).append( '<div class="personality-trait">#'+ response.player.currentCharacter.traitOne + '</div>' );
                  $( '#character-personality-traits' ).append( '<div class="personality-trait">#'+ response.player.currentCharacter.traitTwo + '</div>' );
                  $( '#character-personality-traits' ).append( '<div class="personality-trait">#'+ response.player.currentCharacter.traitThree + '</div>' );

                  // Empty "chatroom-messages". 

                  $( '#chatroom-messages' ).empty();

                  // A sequence of facilitating text animation for "assign character to player".  
                  
                  charactersAssignedAnimation(response);
            });


            /* A sequence of facilitating text animation for "assign character to player". */

            function charactersAssignedAnimation(response) {
                  
                  $( '#facilitator-overlay' ).fadeOut( "slow", function() {

                        // Change "facilitator-text" of the "facilitator-overlay" to "characters-assigned-facilitator-text".

                        $( '.facilitator-text' ).text( response.facilitator.textOne.TEXT );               
                        $( '.facilitator-text' ).attr("id", response.facilitator.textOne.ID);

                        // "characters-assigned-facilitator-text"

                        $( '#facilitator-overlay' ).fadeIn("slow", function() {
                                                
                              // "study-character-facilitator-text"

                              setTimeout(function(){

                                    // Change "facilitator-text" of the "facilitator-overlay" to "study-character-facilitator-text".

                                    $( '.facilitator-text' ).text( response.facilitator.textTwo.TEXT );
                                    $( '.facilitator-text' ).attr("id", response.facilitator.textTwo.ID);

                                    setTimeout(function() {

                                          if ($( '#facilitator-overlay' ).length > 0) {
                                                                              
                                                $( '#facilitator-overlay' ).remove(); $( '#publish-chat-message' ).hide();

                                                // Append "facilitator-notification" to "facilitator-notification-section".

                                                $( '#facilitator-notification-section' ).append( '<p class="facilitator-notification" id="'+ response.facilitator.textTwo.ID +'">' + response.facilitator.textTwo.TEXT +'</p>' );

                                                setTimeout(function() {

                                                      // Remove ".facilitator-notification" message.
                                                      
                                                      if ($( '.facilitator-notification' ).length > 0)
                                                            $( '.facilitator-notification' ).remove();

                                                      // Emit an event to the server to select a player to ask a question. 

                                                      if ( response.player.chatroomOwner ) {

                                                            socket.emit('select player to ask question');
                                                      }

                                                }, 7000);
                                          }

                                    }, 2500); // --> fadeIn effect

                              }, 2500); // --> fadeIn effect  

                              // "study-character-facilitator-text"
                        });

                        // "characters-assigned-facilitator-text"
                  });
            }


            /* Append "facilitator-overlay" with "turn-broadcast-facilitator-text". */

            socket.on('facilitator-turn-broadcast', function(response) {

                  // Remove "facilitator-overlay", if exists.

                  if ( $('#facilitator-overlay').length > 0 )                              
                        $( '#facilitator-overlay' ).remove();

                  // Append "facilitator-overlay" with "turn-broadcast-facilitator-text".

                  $( 'body' ).append( '<div id="facilitator-overlay"> <div class="facilitator-text" id="'+ response.facilitator.ID +'">'+ response.facilitator.TEXT +'</div></div>');

                  setTimeout(function() {
                                                      
                        if ($('.topic-question').length > 0)
                              $('.topic-question').remove();

                        $( '#facilitator-overlay' ).remove(); $( '#publish-chat-message' ).show();

                        // Remove "facilitator-notification" message.
                                                      
                        if ($( '.facilitator-notification' ).length > 0)
                              $( '.facilitator-notification' ).remove();

                        // Append "facilitator-notification" to "facilitator-notification-section".

                        $( '#facilitator-notification-section' ).append( '<p class="facilitator-notification" id="'+ response.facilitator.ID +'">' + response.facilitator.TEXT +'</p>' );
                  
                  }, 2700);
            });

            
            /* Append "facilitator-overlay" with "turn-individual-facilitator-text". */

            socket.on('facilitator-turn-individual', function(response) {

                  // Remove "facilitator-overlay", if exists.

                  if ( $('#facilitator-overlay').length > 0 )                              
                        $( '#facilitator-overlay' ).remove();

                  // Append "facilitator-overlay" with "turn-individual-facilitator-text".

                  $( 'body' ).append( '<div id="facilitator-overlay"> <div class="facilitator-text" id="'+ response.facilitator.ID +'">'+ response.facilitator.TEXT +'</div></div>');
            
                  setTimeout(function() {
                                                      
                        if ($('.topic-question').length > 0)                  
                              $('.topic-question').remove();

                        // Append "topic-question" to "dynamic-topic-questions".

                        response.topicQuestions.forEach(function(question) {

                              $( '#dynamic-topic-questions' ).append( $( '<button id="'+ question.topicQuestionId +'" class="topic-question">'+  question.topicQuestion  + '</button>').on('click', publishTopicQuestion ));
                        });

                        $( '#facilitator-overlay' ).remove(); $( '#publish-chat-message' ).show();
                  
                        // Remove "facilitator-notification" message.
                                                      
                        if ($( '.facilitator-notification' ).length > 0)
                              $( '.facilitator-notification' ).remove();

                        // Append "facilitator-notification" to "facilitator-notification-section".

                        $( '#facilitator-notification-section' ).append( '<p class="facilitator-notification" id="'+ response.facilitator.ID +'">' + response.facilitator.TEXT +'</p>' );

                  }, 2700);
            });


            /* Publish dynamic topic question to "chatroom-messages". */

            function publishTopicQuestion() {

                  socket.emit('process chat message', $( this ).text(), function(response) {
                        
                        if( response.result ) {

                              $( '#chat-message-box' ).val('');

                              // Remove "facilitator-notification" message.
                                                      
                              if ($( '.facilitator-notification' ).length > 0)
                                    $( '.facilitator-notification' ).remove();

                              // Remove "topic-question".

                              if ($('.topic-question').length > 0)                  
                                    $('.topic-question').remove();
                        }
                        else {     
                              
                              // Reset "chat-message-box". 

                              $( '#chat-message-box' ).val('');
                              
                              // Remove "facilitator-overlay", if exists.          
                              
                              if ( $('#facilitator-overlay').length > 0 )                              
                                    $( '#facilitator-overlay' ).remove();

                              // Append "facilitator-overlay" with "question-already-asked-facilitator-text". 
                              
                              $( 'body' ).append( '<div id="facilitator-overlay"> <div class="facilitator-text" id="'+ response.facilitator.ID +'">' + response.facilitator.TEXT +'</div></div>');

                              setTimeout(function(){

                                    $('#facilitator-overlay').remove();

                              }, 2000);
                        }
                  });
            }

            
            /* Publish chat message (reply or question) to "chatroom-messages". */

            $( "#publish-chat-message" ).click(function() {

                  socket.emit('process chat message', $( '#chat-message-box' ).val(), function(response) { 

                        if(response.result) {
                              
                              $( '#chat-message-box' ).val('');

                              if ($( '#turn-individual-facilitator-text' ).length > 0)
                                    $( '#turn-individual-facilitator-text' ).remove();

                              // Remove "topic-question".

                              if ($('.topic-question').length > 0)                  
                                    $('.topic-question').remove();
                        }
                        else {

                              // Reset "chat-message-box".

                              $( '#chat-message-box' ).val('');
                                          
                              // Remove "facilitator-overlay", if exists.

                              if ( $('#facilitator-overlay').length > 0 )                              
                                    $( '#facilitator-overlay' ).remove();

                              // Append "facilitator-overlay" with "question-already-asked-facilitator-text", "no-question-asked-facilitator-text", "reply-aready-sent-facilitator-text".

                              $( 'body' ).append( '<div id="facilitator-overlay"> <div class="facilitator-text" id="'+ response.facilitator.ID +'">' + response.facilitator.TEXT +'</div></div>');
                              
                              setTimeout(function(){

                                    $('#facilitator-overlay').remove();

                              }, 2000);
                        }
                  });
            }); 


            /* Receives a callback from the server if a new chat message has been published. */

            socket.on('new message', function(response) {

                  // Append the chat message to "chatroom-messages".

                  $('#chatroom-messages').append('<div id="'+ response.type +'" style="background-color:'+ response.color +'">'+ response.message +'</div>');
                  $('#chatroom-messages').scrollTop($('#chatroom-messages')[0].scrollHeight);

                  if(response.lastMessage) {

                        setTimeout(function() {

                              // Remove "facilitator-overlay", if exists.

                               if ( $('#facilitator-overlay').length > 0 )                              
                                    $( '#facilitator-overlay' ).remove();

                              // Append "facilitator-overlay" with "turn-ended-facilitator-text", "round-ended-facilitator-text".

                              $( 'body' ).append( '<div id="facilitator-overlay"> <div class="facilitator-text" id="'+ response.facilitator.ID +'">' + response.facilitator.TEXT +'</div></div>');

                              setTimeout(function() {

                                    $('#facilitator-overlay').remove();

                                    // Remove ".facilitator-notification" message.
                                                      
                                    if ($( '.facilitator-notification' ).length > 0)
                                          $( '.facilitator-notification' ).remove();

                                    // Append "facilitator-notification" to "facilitator-notification-section".

                                    if(response.facilitator.ID != "round-ended-facilitator-text" ) {
                                          $( '#facilitator-notification-section' ).append( '<p class="facilitator-notification" id="'+ response.facilitator.ID +'">' + response.facilitator.TEXT +'</p>' );
                                    }

                                    setTimeout(function() { 

                                          // Emit an event to the server to select a player to ask a question or to get the list of characters to guess from (if round has ended). 

                                          if(response.chatroomOwner) {

                                                socket.emit(response.resolution);
                                          }

                                    }, response.timeoutTimer);

                              }, 2500);

                        }, 2000); // -> Time elapsed between last message received and facilitator text popup.
                  }
            });


            /* ----------------- guess who is who ----------------- */


            var characterAndColorMatch = { }, selectedColor = 'rgb(255, 255, 255)';
           
            socket.on( 'select who is who', function(response) {

                  // Create the 'select who is who' layout.

                  setTimeout(function() {

                        // Set up the div's as rows.

                        var firstRow = $( '<div id="first-row" class="row"></div>' ), secondRow = $( '<div id="second-row" class="row"></div>' ), thirdRow = $( '<div id="third-row" class="row"></div>' ), fourthRow = $( '<div id="fourth-row" class="row"></div>' ), fifthRow = $( '<div id="fifth-row" class="row"></div>' );

                        // Populating first row with characters.
                        
                        response.characters.slice(0, 3).forEach(function(character) {

                              characterAndColorMatch[character] = "rgb(255, 255, 255)";
                              firstRow.append( $('<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"></div>').append($('<div class="character-to-guess-from" id="'+ character +'" style="content: url(' + character + ');"></div>').on('click', selectCharacterButton )) );
                        });

                        // Populating second row with characters.

                        response.characters.slice(3, 6).forEach(function(character) {

                              characterAndColorMatch[character] = "rgb(255, 255, 255)";
                              secondRow.append( $('<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"></div>').append($('<div class="character-to-guess-from" id="'+ character +'" style="content: url(' + character + ');"></div>').on('click', selectCharacterButton )) );
                              
                        });

                        // Populating third row with characters.

                        response.characters.slice(6, 9).forEach(function(character) {

                              characterAndColorMatch[character] = "rgb(255, 255, 255)";
                              thirdRow.append( $('<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"></div>').append($('<div class="character-to-guess-from" id="'+ character +'" style="content: url(' + character + ');"></div>').on('click', selectCharacterButton )) );
                        });


                        // Attach player colors.

                        var extraDivElement = $('<div id="guess-color-group" class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12"></div>');

                        response.colors.forEach(function(color) {

                              extraDivElement.append( $('<div class="player-color" style="display: inline-block; background-color:' + color + '; border-color:' + color + ' ;"> </div>').on('click', selectColorButton ));
                        });

                        extraDivElement.append( $('<button id="send-guesses-button">Ok!</button>').on('click', sendGuessesButton ));

                        fourthRow.append(extraDivElement);

                        // Append everything to a bootstrap container.
                        
                        var bootstrapContainer = $('<div class="container container-guess-who-is-who"></div>');

                        bootstrapContainer.append($('<div class="row"><p id="guess-who-is-who-knock-knock-logo"> GUESS WHO IS WHO?! </p></div>'));
                        bootstrapContainer.append(firstRow); bootstrapContainer.append(secondRow); bootstrapContainer.append(thirdRow); bootstrapContainer.append(fourthRow);
                        
                        fifthRow.append($('#chatroom-messageing'));
                        bootstrapContainer.append(fifthRow);

                        // Append to body.

                        $( 'body' ).append( $( '<div id="facilitator-overlay-guessing-layout"></div>' ) );
                        $( '#facilitator-overlay-guessing-layout' ).append(bootstrapContainer);

                  }, 1000);

                  // Remove ".facilitator-notification" message and "topic-question".
                                                      
                  if ($( '.facilitator-notification' ).length > 0)
                        $( '.facilitator-notification' ).remove();

                  if ($('.topic-question').length > 0)                  
                        $('.topic-question').remove();

                  // Reset "chat-message-box".

                   $( '#chat-message-box' ).val('');

                  // Hide the overflow of the main body until player selects who is who. The overlay also gets an overflow. 

                  $( 'body' ).css('overflow', 'hidden');
            });


            /* Select player color. */

            function selectColorButton() {

                  selectedColor = $(this).css('backgroundColor');
            }


            /* Select player character. */

            function selectCharacterButton(){

                  var selectedCharacter = $( this ).css('content').slice(5,-2);

                  $.each(characterAndColorMatch, function( key ) {

                        if (characterAndColorMatch[key] === selectedColor && selectedColor != "rgb(255, 255, 255)" 
                              && key != selectedCharacter) {

                              // Pair selected character with selected color.

                              $('[id="'+ key +'"]').css('border-color', "rgb(255, 255, 255)");                              
                              characterAndColorMatch[key] = "rgb(255, 255, 255)";
                        }
                  });

                  $( this ).css('border-color', selectedColor);

                  characterAndColorMatch[selectedCharacter] = selectedColor;
            }


            /* Emit an event to the server to send the guesses that the player has made. */ 

            function sendGuessesButton() {

                  $( '#send-guesses-button' ).prop('disabled', true);

                  socket.emit('give player points', characterAndColorMatch, function(response) {

                        $( '#send-guesses-button' ).remove();

                        // Show the "overflow" of the main body.

                        $( 'body' ).css('overflow', 'visible');

                        // Append the "chatroom-messageing" back to the "chatroom-messageing-section".

                        $('#chatroom-messageing-section').append( $('#chatroom-messageing') ); 

                        // Remove "facilitator-overlay-guessing-layout", if exists.

                        if ( $( '#facilitator-overlay-guessing-layout' ).length > 0 )                              
                              $( '#facilitator-overlay-guessing-layout' ).remove();

                        // Append "facilitator-overlay" with "waiting-players-to-guess-facilitator-text".

                        $( 'body' ).append( '<div id="facilitator-overlay"> <div class="facilitator-text" id="'+ response.facilitator.ID +'">'+ response.facilitator.TEXT +'</div></div>');
                  });
            }


            /* ----------------- guess who is who ----------------- */


            /* Append the other players points inside the "player-color". */

            socket.on('broadcast player points', function(response) {

                  $( '#'+ response.socketId).children( ".ready" ).text(response.points);
            });


            /* Receives a callback from the server with the number of points based on the guesses made. */

            socket.on('callback player points', function(response) {

                  // Remove "current-player-points", if exists.

                  if ($('#current-player-points').length > 0)
                        $('#current-player-points').remove();

                  // Append "current-player-points", below the "assigned-character-color".
                  
                  $( '#assigned-character-color' ).append( '<span id="current-player-points">'+ response.points +'</span>' );
            });


            /* After each guessing round, this socket event receives a callback from the server with all the points of each player. */

            socket.on('points dashboard', function(response) {

                  // Append the "chatroom-messageing" back to the "chatroom-messageing-section".

                  $('#chatroom-messageing-section').append( $('#chatroom-messageing') ); 

                  // Remove "facilitator-overlay-guessing-layout", if exists.
                  
                  if ( $( '#facilitator-overlay-guessing-layout' ).length > 0 )                              
                        $( '#facilitator-overlay-guessing-layout' ).remove();

                  // Remove "facilitator-overlay", if exists.

                  if ( $('#facilitator-overlay').length > 0 )                              
                        $( '#facilitator-overlay' ).remove();

                  // Hide the main "container" of the "post-chatroom-messaging.html" page.
                  
                  $( ".container" ).hide();

                  
                  var playerPointsDashboard = $('<div id="player-points-dashboard"></div>');

                  playerPointsDashboard.append($('<p id="player-points-dashboard-knock-knock-logo"> POINTS DASHBOARD </p>'));

                  
                  var extraDivElement = $('<div id="player-points-group"></div>');

                  response.players.forEach(function(player) {

                        extraDivElement.append('<div id="player-points" style="color: '+ player.color +';" >'+ player.colorName +': '+ player.points +'</div>');
                  });

                  playerPointsDashboard.append(extraDivElement);
                  
                  
                  // If the player is the chatroom owner than advance the game session.

                  if(response.chatroomOwner) {

                        // If the current round is the last round then finish session else start another round.

                        if(!response.lastRound)
                              playerPointsDashboard.append( $('<button id="start-round-button">START ROUND</button>').on('click', startRoundButton ));
                        else
                              playerPointsDashboard.append( $('<button id="finish-session-button">FINISH SESSION</button>').on('click', finishSessionButton ));                      
                  }

                  $( 'body' ).append(playerPointsDashboard);
            });


            /* This function starts a round. */

            function startRoundButton() {

                  $( '#start-round-button' ).prop('disabled', true);

                  socket.emit('next round', function() {

                        $( '#start-round-button' ).remove();
                  });
            }


            /* This function finishes the game session that the chatroom owner has started. */

            function finishSessionButton() {

                  $( '#finish-session-button' ).prop('disabled', true);

                  location.reload(true);
            };
      });


      /* ----------------- post-chatroom-messaging ----------------- */
});