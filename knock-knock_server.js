

/*
*
* Copyright (C) 2018 Tamás Lakatos <taml@itu.dk>
* 
* This file is part of "knock-knock_game" and cannot be copied and/or distributed without the express permission of the following 
* persons: Tamás Lakatos <taml@itu.dk>, Sissel Angela Ringvad <sanr@itu.dk> and Sarah Grossi <sagr@itu.dk>.
* 
*/


const 
      // Loading external modules.

      EXPRESS_FRAMEWORK = require('express'),
      SOCKET_IO_CLIENT = require('socket.io'),
      AWS_CLIENT = require('aws-sdk'),

      // Initializing Socket.IO and Express.

      EXPRESS_APPLICATION = EXPRESS_FRAMEWORK(),
      PORT_NUMBER = 3000,
      EXPRESS_SERVER = EXPRESS_APPLICATION.listen(PORT_NUMBER),
      IO = SOCKET_IO_CLIENT.listen(EXPRESS_SERVER),
      
      // AWS credentials for accessing S3 and DynamoDB API services.

      AWS_ACCESS_CREDENTIALS = {     
            accessKeyId: 'AKIAINKHWDHWLJXJXE7Q',  
            secretAccessKey: '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',  
            region: 'eu-west-1'
      },

      // Setting up a collection of key value pairs for facilitating the gameplay. 

      FACILITATING = Object.freeze({
            
            ROUND_STARTED: { TEXT: 'Round is starting!', ID: 'roud-started-facilitator-text' },
            CHARARCTERS_ASSIGNED: { TEXT: 'You have been assigned a character!', ID: 'characters-assigned-facilitator-text' },
            STUDY_CHARACTER: { TEXT: 'Study your character for 5 seconds!', ID: 'study-character-facilitator-text' },
            TURN_BROADCAST: { TEXT: "'s turn to ask a question!", ID: 'turn-broadcast-facilitator-text' },
            TURN_INDIVIDUAL: { TEXT: 'Your turn to ask a question!', ID: 'turn-individual-facilitator-text' },
            QUESTION_ALREADY_ASKED: { TEXT: 'You already asked a question!', ID: 'question-already-asked-facilitator-text' },          
            NO_QUESTION_ASKED: { TEXT: 'Nobody asked a question yet!', ID: 'no-question-asked-facilitator-text' },
            REPLY_ALREADY_SENT: { TEXT: 'You already texted your reply!', ID: 'reply-aready-sent-facilitator-text' },
            TURN_ENDED: { TEXT: 'Consider the aswers for 5 seconds!',  ID: 'turn-ended-facilitator-text' },
            ROUND_ENDED: { TEXT: 'Round has ended! - Guess Who Is Who?!', ID: 'round-ended-facilitator-text' },
            WAITING_PLAYERS_TO_GUESS: { TEXT: 'Other players are still guessing!', ID: 'waiting-players-to-guess-facilitator-text' },

            NO_OWNER_JOINED_CHATROOM: { TEXT: 'The chat room owner has not joined yet!', ID: 'no-owner-joined-chatroom-facilitator-text' },
            GAME_ALREADY_STARTED: { TEXT: 'The game already started!', ID: 'game-already-started-facilitator-text' },
            CHATROOM_IS_FULL: { TEXT: 'The chat room is already full!', ID: 'chatroom-is-full-facilitator-text' },
            LATE_TO_JOIN_CHATROOM: { TEXT: 'Time is up. Create a new chat room and join again!', ID: 'late-to-join-chatroom-facilitator-text' },
            CHATROOM_ALREADY_IN_USE: { TEXT: 'This pin you have created is currently being used!', ID: 'chatroom-already-in-use-facilitator-text' },
            INVALID_CHATROOM_ID: { TEXT: 'We did not recognize this chat room. Check and try again!', ID: 'invalid-chatroom-id-facilitator-text' },
            NOT_ENOUGHT_PLAYERS: { TEXT: 'There are not enought players to start a session! Wait for others to join!', ID: 'not-enought-players-facilitator-text' }
      }),

      // Declaring various "enums".
      
      COLORS = Object.freeze({ 'Mona Lisa' : 'rgb(255, 153, 153)', 'Lime Green': 'rgb(50, 205, 50)', 'Magic Mint': 'rgb(153, 255, 204)', 'Dodger Blue':  'rgb(51, 153, 255)', 'Banana Mania': 'rgb(255, 240, 179)' }),
      ROUND = Object.freeze({ ONE: 0, TWO: 1, THREE: 2 }),
      MESSAGE_TYPE = Object.freeze({ PRECHAT: 'pre-chat', QUESTION: 'question', REPLY: 'reply', INVALID: 'invalid' }),
      QUESTION = Object.freeze({ ONE: 0, TWO: 1 }),

      // Initializing global variables.

      SOCKET_CONNECTIONS = [],
      CHATROOMS = { },

      NUMBER_OF_QUESTIONS_TO_ASK = 2,
      NUMBER_OF_CHARACTERS_TO_GUESS = 9,

      MAX_NUMBER_OF_PLAYERS = 5,
      MIN_NUMBER_OF_PLAYERS_READY = 3,
      ASKED_QUESTION_COLOR = '#ffe441';


EXPRESS_APPLICATION.use('/knock-knock_app.min.js', EXPRESS_FRAMEWORK.static(__dirname + '/app/knock-knock_app.min.js'));
EXPRESS_APPLICATION.use('/jquery-3.3.1.min.js', EXPRESS_FRAMEWORK.static(__dirname + '/app/jquery-3.3.1.min.js'));
EXPRESS_APPLICATION.use('/bootstrap.min.css', EXPRESS_FRAMEWORK.static(__dirname + '/app/bootstrap.min.css'));
EXPRESS_APPLICATION.use('/knock-knock_styles.min.css', EXPRESS_FRAMEWORK.static(__dirname + '/app/knock-knock_styles.min.css'));
EXPRESS_APPLICATION.use('/post-chatroom-messaging.html', EXPRESS_FRAMEWORK.static(__dirname + '/app/post-chatroom-messaging.html'));
EXPRESS_APPLICATION.use('/favicon.png', EXPRESS_FRAMEWORK.static(__dirname + '/app/favicon.png'));
EXPRESS_APPLICATION.use('/logos/21030018_126001.png', EXPRESS_FRAMEWORK.static(__dirname + '/app/logos/21030018_126001.png'));
EXPRESS_APPLICATION.use('/logos/43070045_127008.png', EXPRESS_FRAMEWORK.static(__dirname + '/app/logos/43070045_127008.png'));


/* Sending pre-chatroom-join.html when a player launches the game. */

EXPRESS_APPLICATION.get('/', function(req, res) {

      // Send pre-chatroom-join.html for root GET request.

      res.sendFile(__dirname + '/app/pre-chatroom-join.html');
});


/* The following custom socket events can be triggered by the client knock-knock_app after connecting to the knock-knock_server. */

IO.sockets.on('connection', function(socket) {

      SOCKET_CONNECTIONS.push(socket);

      console.log('Socket with ' + socket.id + ' connected to knock-knock_server!');
      console.log('Number of sockets currently connected on the knock-knock_server: ' + SOCKET_CONNECTIONS.length);

      // Creates a chatroom with a unique chatroom id.
    
      socket.on('create chatroom', (callback) => createChatroom(socket, callback));

      // Validates the chatroom id enterd by the player.

      socket.on('validate chatroom id', (chatroomId, callback) => validateChatroomId(chatroomId, socket, callback));

      // Puts the player in a chatroom.

      socket.on('join chatroom', (chatroomId, callback) => joinChatroom(chatroomId, socket, callback));

      // Validates whether there are enought players to start a session.

      socket.on('check number of players', (callback) => checkNumberOfPlayers(socket, callback));

      // Signals to everybody in the chatroom that a player is ready to start the game.

      socket.on('ready to play', (callback) => readyToPlay(socket, callback));

      // This event can only be triggered by the chatroom owner when there are at least three players ready to start the game.

      socket.on('start session', (callback) => startSession(socket, callback));

      // Selects and specifies a players turn to ask a question.
      
      socket.on('select player to ask question', () => selectPlayerToAskQuestion(socket));

      // Processes all incomming chat messages.
      
      socket.on('process chat message', (message, callback) => processChatMessage(message, socket, callback));

      // Sends nine characters from which, the player must guess the other players characters.

      socket.on('guess who is who', () => guessWhoIsWho(socket));

      // Gives points to players based on how well they have role played their characters and guessed other players characters.

      socket.on('give player points', (guesses, callback) => givePlayerPoints(guesses, socket ,callback));

      // Starts the next round.

      socket.on('next round', (callback) => nextRound(socket, callback));

      // Disconnects the player socket connection from the knock-knock_server. 

      socket.on('disconnect', () => disconnectPlayer(socket));
});


/* This function creates an empty chatroom with a unique id and assigns the socket (that has triggered this function) as its owner. */

function createChatroom (socket, callback) {
 
      const NUMBER_OF_DIGITS = 4, POSSIBLE_DIGITS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

      let chatroomId = "";

      // Create a chatroomId of a four digit long string.

      for (i = 0; i < NUMBER_OF_DIGITS; i++)
            chatroomId += POSSIBLE_DIGITS.charAt(Math.floor(Math.random() * POSSIBLE_DIGITS.length));

      // Check whether the chatroom is currently in use.

      if (!(chatroomId in CHATROOMS)) {

            // Create an empty chatroom with a unique chatroom id.

            CHATROOMS[chatroomId] = { };

            console.log('Chatroom with ' + chatroomId + ' chatroom id created!');

            CHATROOMS[chatroomId].owner = socket.id;
            CHATROOMS[chatroomId].ownerHasJoined = false;
            CHATROOMS[chatroomId].gameStarted = false;
            CHATROOMS[chatroomId].players = [];
            CHATROOMS[chatroomId].messages = [];
            CHATROOMS[chatroomId].colors = [];
            CHATROOMS[chatroomId].rounds = { };

            // Send the chatroomId to the player.

            callback({ result: true, chatroomId: chatroomId }); console.log('Chatroom id '+ chatroomId + ' sent to player!');

            // Delete the chatroom after twenty seconds if the "owner of the chatroom" has not joined.  

            setTimeout(function(){

                  if ((chatroomId in CHATROOMS) && CHATROOMS[chatroomId].players.length <= 0) {

                        delete CHATROOMS[chatroomId]; 

                        console.log('Deleted chatroom with ' + chatroomId + ' chatroom id!');

                        IO.to(socket.id).emit('too late to join chatroom', { facilitator: FACILITATING.LATE_TO_JOIN_CHATROOM });
                  }

            }, 60000);
      }
      else {

            // Send callback if the chatroom is currently being used. 

            callback({ result: false, facilitator: FACILITATING.CHATROOM_ALREADY_IN_USE });  
      }
}


/* This function validates whether the chatroomId that the player has submited is valid. */ 

function validateChatroomId (chatroomId, socket, callback) {

      console.log('Validating chatroomId: ' + chatroomId);
      
      // Check wheter the chatroom that the player is trying to join, is already created.

      if(!(chatroomId in CHATROOMS)) {

            callback({ result: false, facilitator: FACILITATING.INVALID_CHATROOM_ID }); 
            return;
      }

      // Check wheter the chatroom owner has already joined the chatroom.

      if(!CHATROOMS[chatroomId].ownerHasJoined && socket.id != CHATROOMS[chatroomId].owner) {

            callback({ result: false, facilitator: FACILITATING.NO_OWNER_JOINED_CHATROOM });
            return;
      }

      // Check whether the game has already started.

      if (CHATROOMS[chatroomId].gameStarted) {

            callback({ result: false, facilitator: FACILITATING.GAME_ALREADY_STARTED });            
            return;
      }

      // Check whether the number of players that are already in the chatroom are less than the max number of players allowed.

      if (CHATROOMS[chatroomId].players.length >= MAX_NUMBER_OF_PLAYERS) {

            callback({ result: false, facilitator: FACILITATING.CHATROOM_IS_FULL });            
            return;
      }

      // Send callback to player if everything is fine.

      callback({ result: true, chatroomId: chatroomId });
}


/* This function places the player in a chatroom based on the chatroomId, and it also assignes a random color to the player. */

function joinChatroom(chatroomId, socket, callback) {
      
      let player = {

            socketId: socket.id,
            chatroomId: chatroomId,
            points: 0,
            chatroomOwner: false,
            charactersRolePlayed: [],
            readyToPlay: false
      };

      // Check whether the player that is joining the chatroom is the owner of the chatroom.

      if (player.socketId === CHATROOMS[chatroomId].owner) {

            player.chatroomOwner = true;
            player.readyToPlay = true;

            CHATROOMS[chatroomId].ownerHasJoined = true;
      }

      // Filter colors against all the colors that have already been assigned. 

      let colors = Object.keys(COLORS).filter(key => !CHATROOMS[chatroomId].colors.includes(key));

      // Assign a random color to the player.

      player.color = colors[Math.floor(Math.random() * colors.length)];

      console.log(player.color + ' color assigned to player!');
      
      CHATROOMS[chatroomId].colors.push(player.color); CHATROOMS[chatroomId].players.push(player);

      // Put player in chatroom.
      
      socket.room = chatroomId; socket.join(chatroomId);
      
      console.log('Put player in chatroom: ' + chatroomId);

      // Information about the players color and the other players color, are sent to the player that has joined the chatroom.

      callback({ chatroomId: player.chatroomId, player: { socketId: player.socketId, chatroomOwner: player.chatroomOwner, color: COLORS[player.color] }, messages: CHATROOMS[chatroomId].messages, 
                        otherPlayers: CHATROOMS[chatroomId].players.filter(p => p.socketId != socket.id).map( p => { return { socketId: p.socketId, color: COLORS[p.color], readyToPlay: p.readyToPlay } }, [] ) });

      // Broadcast the player color for all the other players, to create awareness that a new player has joined the chatroom.  

      socket.broadcast.to(socket.room).emit('broadcast player joined chatroom', { socketId: player.socketId, color: COLORS[player.color] });
}


/* This function verifies whether there are enought players to start a session. */

function checkNumberOfPlayers(socket, callback) {

	(CHATROOMS[socket.room].players.filter(p => p.readyToPlay === true).length >= MIN_NUMBER_OF_PLAYERS_READY)
            ? callback({ result: true }) : callback({ result: false, facilitator: FACILITATING.NOT_ENOUGHT_PLAYERS });
}


/* This function sets and creates awareness that a player is ready to start a session. */

function readyToPlay(socket, callback) {

      // Find player with the specific socketId and set player ready to play.

      let player = CHATROOMS[socket.room].players.find(p => p.socketId === socket.id);
      player.readyToPlay = true;

      console.log('Player ' + player.socketId + ' is ready to play!'); callback();

      // Broadcast awareness that a player is ready to start a session.
      
      socket.broadcast.to(socket.room).emit('awareness on player ready', player.socketId);
}


/* This function starts a game session and removes all other players that are not ready to play. */

function startSession(socket, callback) {

      // Filter players that are not ready to start a game session, and then foreach player. 

      CHATROOMS[socket.room].players.filter(p => p.readyToPlay === false).forEach(function(playerNotReady) {

            // Remove disconnected player from the chatroom.

            CHATROOMS[socket.room].players.splice(CHATROOMS[socket.room].players.indexOf(playerNotReady), 1);
            CHATROOMS[socket.room].colors.splice(CHATROOMS[socket.room].colors.indexOf(playerNotReady.color), 1);

            // Find the socket abstraction of the player and set its "room" property to undefined.

            let playerSocket = SOCKET_CONNECTIONS.find(s => s.id === playerNotReady.socketId);
            IO.sockets.sockets[playerSocket.id].room = undefined;

            // Emit a socket event to player to leave the chatroom. This will redirect the player to the pre-chatroom-join.html page.

            IO.to(playerNotReady.socketId).emit('remove player from chatroom');

            IO.sockets.to(socket.room).emit('remove player from chatroom feedback', playerNotReady.socketId);
      });

      // Set game started to true.

      CHATROOMS[socket.room].gameStarted = true;

      // Emit facilitating text to all players that a round is starting.

      IO.sockets.to(socket.room).emit('facilitator-round-started', { facilitator: FACILITATING.ROUND_STARTED });

      // Start the first round of the game session.
      
      startRound(ROUND.ONE, socket);
}


/* This function initializes everything needed to start a round. */ 

function startRound(roundNumber, socket) {

      // Dynamically create a round.

      CHATROOMS[socket.room].rounds[roundNumber] = { };
      CHATROOMS[socket.room].rounds[roundNumber].characters = [];
      CHATROOMS[socket.room].rounds[roundNumber].messages = [];
      
      // Set the current round inside the chatroom. 

      CHATROOMS[socket.room].currentRound = roundNumber;

      // Reset chatroom parameters.

      CHATROOMS[socket.room].characters = [];
      CHATROOMS[socket.room].characterPhotographs = [];
      CHATROOMS[socket.room].turnToAskQuestion = "";
      CHATROOMS[socket.room].alreadyAssignedTopicQuestions = [];
      CHATROOMS[socket.room].guessingSession = false;

      // Reset player parameters.

      CHATROOMS[socket.room].players.forEach( p => p.alreadyAskedQuestion = Array.of(false, false) );
      CHATROOMS[socket.room].players.forEach( p => p.guessedWhoIsWho = false);
      CHATROOMS[socket.room].players.forEach( p => p.currentTopicQuestions = []);
      CHATROOMS[socket.room].players.forEach( p => p.currentPoints = 0);
      CHATROOMS[socket.room].players.forEach( p => p.currentQuestion = -1);

      // Assign a character with personality traits and two topic questions foreach player.

      Promise.all(CHATROOMS[socket.room].players.map(assignCharacterAndQuestions)).then(function(players) {

            CHATROOMS[socket.room].rounds[roundNumber].characters = CHATROOMS[socket.room].characters;

            // Emit character with personality traits, topic questions and facilitating text to each player.

            CHATROOMS[socket.room].players.forEach(function(player) {
                  
                  IO.to(player.socketId).emit('assign character to player', { player: { socketId: player.socketId, chatroomOwner: player.chatroomOwner, currentCharacter: player.currentCharacter },
                        facilitator: { textOne: FACILITATING.CHARARCTERS_ASSIGNED, textTwo: FACILITATING.STUDY_CHARACTER } });
            });
      });
}


/* This function assignes a character and two topic questions to the player. */

function assignCharacterAndQuestions(player) {
 
 	// Return a promise.
      
      return new Promise(function(resolve) {

            const 

            	// Initializing S3 and DYNAMODB client constant variables.

            	DYNAMODB = new AWS_CLIENT.DynamoDB(AWS_ACCESS_CREDENTIALS),
                  S3 = new AWS_CLIENT.S3(AWS_ACCESS_CREDENTIALS),

                  // Initializing S3 and DYNAMODB client request parameters.
                  
                  PARAMETERS_S3 = { Bucket: 'knock-knock-character-assets' },
                  PARAMETERS_DYNAMODB_PERSONALITY_TRAITS = { TableName: 'who-are-you-character-personality-traits' },
                  PARAMETERS_DYNAMODB_QUESTIONS = { TableName: 'who-are-you-topic-question-assets' },
                                   
                  // Pre-process all assets before assigning them to the players. 

                  LIST_QUESTIONS = DYNAMODB.scan(PARAMETERS_DYNAMODB_QUESTIONS).promise().then(function(topicQuestions) {

                  	// Map all the topic questions to an array of key/value javascipt objects, and filter the questions that have already been assigned.
                  	
                  	return fisherYatesShuffle(topicQuestions.Items.map(function(question) { return  { topicQuestionId : question.topicQuestionId.N, topicQuestion : question.topicQuestion.S }; }).filter( q => !CHATROOMS[player.chatroomId].alreadyAssignedTopicQuestions.includes(q.topicQuestionId) ));
                  }),
      
                  LIST_CHARACTERS = S3.listObjects(PARAMETERS_S3).promise().then(function(characters) {
                        
                  	// Filter all the characters, that are not already taken for the current round or have not been role played in previous rounds, by the current player.

                  	return fisherYatesShuffle(characters.Contents.filter( c => !CHATROOMS[player.chatroomId].characters.includes(c.Key) ).filter( c => !player.charactersRolePlayed.includes(c.Key) ));
                  });

            // Assign two random topic questions to the player. 

            LIST_QUESTIONS.then(function(topicQuestions) {

            	const NUMBER_OF_TOPIC_QUESTIONS = 2;

            	for(i = 0; i < NUMBER_OF_TOPIC_QUESTIONS; i++) {

            		// Prevent having the same topic question twice.

            		let filteredQuestions = topicQuestions.filter(q => !player.currentTopicQuestions.includes(q) );
            		let question = filteredQuestions[Math.floor(Math.random() * filteredQuestions.length)];

            		// Assign a topic question to player.
            		
            		player.currentTopicQuestions.push(question);
            		
            		// Register topic question as already assigned.

            		CHATROOMS[player.chatroomId].alreadyAssignedTopicQuestions.push(question.topicQuestionId);
            	}
            });

            // Assign a character with personality traits to the player.

            LIST_CHARACTERS.then(function(characters) {

			let CHARARCTER = { }; 

			CHARARCTER.Key = characters[Math.floor(Math.random() * characters.length)].Key;

			// Add the character key to the list of characters that have already been assigned.

                  CHATROOMS[player.chatroomId].characters.push(CHARARCTER.Key);
             
                  PARAMETERS_S3.Key = CHARARCTER.Key;
                  
                  // Get character photographs.

                  S3.getSignedUrl('getObject', PARAMETERS_S3, function(err, url) {

                        CHATROOMS[player.chatroomId].characterPhotographs.push(url);
                        
                        CHARARCTER.URL = url;

                        PARAMETERS_DYNAMODB_PERSONALITY_TRAITS.Key = {'characterKey': { S: CHARARCTER.Key }}; 

                        // Get character personality traits.

                        DYNAMODB.getItem(PARAMETERS_DYNAMODB_PERSONALITY_TRAITS).promise().then(function(traits) {

                              CHARARCTER.traitOne = traits.Item.traitOne.S; CHARARCTER.traitTwo = traits.Item.traitTwo.S; CHARARCTER.traitThree = traits.Item.traitThree.S;

                              // Assign the character to player.

                              player.currentCharacter = CHARARCTER;

                              // Resolve promise.

                              resolve(player);
                        });
                  });
            });     
      });
}


/* This function randomly picks a player to ask a question. */

function selectPlayerToAskQuestion(socket) {

      CHATROOMS[socket.room].players.forEach(p => p.textedChatMessage = false);

      let players = (CHATROOMS[socket.room].players.every(p => p.alreadyAskedQuestion[QUESTION.ONE] === true))
            ? CHATROOMS[socket.room].players.filter(p => p.alreadyAskedQuestion[QUESTION.TWO] != true && CHATROOMS[socket.room].turnToAskQuestion != p.color) : CHATROOMS[socket.room].players.filter(p => p.alreadyAskedQuestion[QUESTION.ONE] != true);
           
      CHATROOMS[socket.room].questionHasBeenAsked = false;

      // Assign a random player to ask a question.

      let playerToAskQuestion = players[Math.floor(Math.random() * players.length)];

      playerToAskQuestion.currentQuestion = (playerToAskQuestion.currentQuestion === -1) ? QUESTION.ONE : QUESTION.TWO;
      CHATROOMS[socket.room].turnToAskQuestion = playerToAskQuestion.color;

      // Send facilitating text to the player, that it is his turn to ask a question.

      IO.to(playerToAskQuestion.socketId).emit('facilitator-turn-individual',
            { topicQuestions: playerToAskQuestion.currentTopicQuestions, facilitator: FACILITATING.TURN_INDIVIDUAL });

      // Send facilitating text to all players, to create awerness of whose turn is it too ask a question.
     
      CHATROOMS[socket.room].players.filter(p => p != playerToAskQuestion).forEach(function(player) {

            IO.to(player.socketId).emit('facilitator-turn-broadcast',
            	{ facilitator: { TEXT: playerToAskQuestion.color + FACILITATING.TURN_BROADCAST.TEXT, ID: FACILITATING.TURN_BROADCAST.ID } });
      });
}


/* This function pre-processes all incoming messages by assigning each a message type, and further routing them to be published. */ 

function processChatMessage(message, socket, callback) {

      let chatMessage = { }, verdict = { };

      // Find player with the specific socketId. 

      let player = CHATROOMS[socket.room].players.find(p => p.socketId === socket.id); 

      // Construct chat message.
      
      chatMessage.playerId = player.socketId;
      chatMessage.color = COLORS[player.color];
      chatMessage.message = message;

      // Set the verdict and message type.

      if (!CHATROOMS[socket.room].gameStarted) {
            
            chatMessage.type = MESSAGE_TYPE.PRECHAT;
            verdict.result = true;
      }
      else {

            if (CHATROOMS[socket.room].questionHasBeenAsked) {

                  if(!player.textedChatMessage) {

                        chatMessage.type = MESSAGE_TYPE.REPLY;
                        verdict.result = true; 
                  }
                  else {

                        if(!player.alreadyAskedQuestion[player.currentQuestion]) {

                              chatMessage.type = MESSAGE_TYPE.INVALID;
                              verdict.facilitator = FACILITATING.REPLY_ALREADY_SENT;
                              verdict.result = false;
                        }
                        else {
                              
                              chatMessage.type = MESSAGE_TYPE.INVALID;
                              verdict.facilitator = FACILITATING.QUESTION_ALREADY_ASKED
                              verdict.result = false;
                        }
                  }

            } else {

                  if(CHATROOMS[socket.room].turnToAskQuestion === player.color) {

                        chatMessage.type = MESSAGE_TYPE.QUESTION;
                        verdict.result = true;
                  } 
                  else {
                        
                        chatMessage.type = MESSAGE_TYPE.INVALID;
                        verdict.facilitator = FACILITATING.NO_QUESTION_ASKED; 
                  }
            }
      }

      // Route chat message based on the message type.

      switch(chatMessage.type) {
            
            case MESSAGE_TYPE.PRECHAT:

                  CHATROOMS[socket.room].messages.push(chatMessage);
                  
                  callback(verdict);
            break;
            
            case MESSAGE_TYPE.QUESTION:
                  
                  CHATROOMS[socket.room].questionHasBeenAsked = true;

                  player.alreadyAskedQuestion[player.currentQuestion] = true;
                  player.textedChatMessage = true;

                  chatMessage.color = ASKED_QUESTION_COLOR;
                  
                  CHATROOMS[socket.room].rounds[CHATROOMS[socket.room].currentRound].messages.push(chatMessage);                  
                  
                  callback(verdict);
            break;
            
            case MESSAGE_TYPE.REPLY:
                  
                  player.textedChatMessage = true;
                  
                  CHATROOMS[socket.room].rounds[CHATROOMS[socket.room].currentRound].messages.push(chatMessage);
                  
                  callback(verdict);
            break;
            
            default:
                  
                  callback(verdict);

                  return;
      }

      // Publish valid chat message. 

      publishChatMessage(chatMessage, socket);
}


/* This function broadcasts all valid incomming chat messages. */ 

function publishChatMessage(message, socket) {

      let sendChatMessage = { };

      // If the type of the message is pre-chat.

      if(message.type === MESSAGE_TYPE.PRECHAT) {

            let chatMessage = CHATROOMS[socket.room].messages.find(m => m === message);

            sendChatMessage = chatMessage;
      }

      // If the type of the message is a question or a reply.

      if(message.type === MESSAGE_TYPE.REPLY || message.type === MESSAGE_TYPE.QUESTION) {

            let chatMessage = CHATROOMS[socket.room].rounds[CHATROOMS[socket.room].currentRound].messages.find(m => m === message);

            sendChatMessage = chatMessage;

            // Check wheter all players ahve texted their message. 

	      if(CHATROOMS[socket.room].players.every(p => p.textedChatMessage === true))
	      {

	            if(CHATROOMS[socket.room].players.every(p => p.alreadyAskedQuestion[QUESTION.ONE] === true 
	      		&& p.alreadyAskedQuestion[QUESTION.TWO] === true)) {

	            	// Send the 'guess who is who' resolution to player.

	            	chatMessage.lastMessage = true;
	            	chatMessage.facilitator = FACILITATING.ROUND_ENDED;
	            	chatMessage.resolution = 'guess who is who';
                        chatMessage.timeoutTimer = 0;

	            } else {

	            	// Send the 'select player to ask question' resolution to player.
	            	
	            	chatMessage.lastMessage = true;
	            	chatMessage.facilitator = FACILITATING.TURN_ENDED;
	            	chatMessage.resolution = 'select player to ask question';
                        chatMessage.timeoutTimer = 9000;
	            }
	      }
	      else {

	            chatMessage.lastMessage = false;
	      }
	}

      // Send chat message to every player in the chatroom.

      CHATROOMS[socket.room].players.forEach(function(player) {

            sendChatMessage.chatroomOwner = player.chatroomOwner;

            IO.to(player.socketId).emit('new message', sendChatMessage );
      });
}


/* This function broadcasts nine characters, from which the player must guess the other players characters. */

function guessWhoIsWho(socket) {

      const 

      	// Initializing S3 client constant variables.

      	S3 = new AWS_CLIENT.S3(AWS_ACCESS_CREDENTIALS),
		PARAMETERS_S3 = { Bucket: 'knock-knock-character-assets' };

	// Send a request to list all the characters from the S3 object storage.

	LIST_CHARACTERS = S3.listObjects(PARAMETERS_S3).promise().then(function(characters) {
          
            // Determine the number of extra characters needed to "camouflage" the characters that are actually being role played.

            let numberOfExtraCharacters = (NUMBER_OF_CHARACTERS_TO_GUESS - 
            	CHATROOMS[socket.room].rounds[CHATROOMS[socket.room].currentRound].characters.length) + 1;

		// Remove all characters that are currently being role played in the current round.

            let remainingCharacters = fisherYatesShuffle(characters.Contents
            	.filter(c => !CHATROOMS[socket.room].characters.includes(c.Key)));

            return remainingCharacters.slice(0, numberOfExtraCharacters);
      });

	// Mix the actual characters that are being role played with the extra characters, then broadcast them to all the players that are in the chatroom.

      LIST_CHARACTERS.then(function(characters) {

            Promise.all(characters.map(characterKeyToURL)).then(function(characters) {

                  CHATROOMS[socket.room].players.forEach(function(player) {

                        // Send a list of character photographs and colors to the player.

                        IO.to(player.socketId).emit('select who is who', { colors: fisherYatesShuffle(CHATROOMS[socket.room].colors.filter(c => c != player.color)).map( c => { return COLORS[c] } , [] ),                 
                              characters: fisherYatesShuffle(characters.concat(CHATROOMS[socket.room].characterPhotographs.filter(c => c != player.currentCharacter.URL)))
                        });
                  });

                  CHATROOMS[socket.room].guessingSession = true;
            });
      });
}


/* This function maps all the character keys to url's. */

function characterKeyToURL(character) {
	
	const 

		S3 = new AWS_CLIENT.S3(AWS_ACCESS_CREDENTIALS),
		PARAMETERS_S3 = { Bucket: 'knock-knock-character-assets' };

	return new Promise(function(resolve) {                  
	
		PARAMETERS_S3.Key = character.Key;
		
		S3.getSignedUrl('getObject', PARAMETERS_S3, function(err, url) {         
		
			resolve(url);
		});
	});
}


/* This function gives each player points based on their guesses. */
 
function givePlayerPoints(guesses, socket, callback) {

	let lastRound = false;

      // Find player with the specific socketId. 

	let currentPlayer = CHATROOMS[socket.room].players.find(p => p.socketId === socket.id);

	// Filter the character photographs that have actually been role played, during the round. 

      let filteredGuesses = Object.keys(guesses).filter(key => CHATROOMS[socket.room].characterPhotographs.includes(key)).reduce((guess, key) => { guess[key] = guesses[key]; return guess; }, { } );
      
      // Foreach guess, check if the player guessed correctly and give points accordingly.

      Object.keys(filteredGuesses).forEach(function(key) {

      	// Find the player that is role playing the character. 

            let player = CHATROOMS[socket.room].players.find(p => p.currentCharacter.URL === key);

            // Check if the color of the player matches the color that the the current player has guessed.  
            
            if( COLORS[player.color] == filteredGuesses[key] ) {

                  // The current player that guessed gets a point, and the player that has been guessed, also gets a point for good role play.

                  currentPlayer.currentPoints += 1;
                  player.currentPoints += 1;
                  
                  currentPlayer.points += 1;
                  player.points += 1;
            }
      });

      // Flag the current player as already guessed who is who, and push the players current character to a list of role played characters.

      currentPlayer.guessedWhoIsWho = true;
      currentPlayer.charactersRolePlayed.push(currentPlayer.currentCharacter.Key);
      
      if(CHATROOMS[socket.room].players.every(p => p.guessedWhoIsWho === true)) {

            sendPlayerPoints(socket, lastRound);
      }
      else {

            // Send callback to player.

            callback({ facilitator: FACILITATING.WAITING_PLAYERS_TO_GUESS });
      }
}


/* This function broadcasts each player points (points dashboard) to all players that are in the current game session. */

function sendPlayerPoints(socket, lastRound) {

      if (CHATROOMS[socket.room].currentRound >= (Object.keys(ROUND).length - 1)) {

            lastRound = true;

            // Broadcast each players points to all other players.
   
            CHATROOMS[socket.room].players.forEach(function(player) {

                  IO.to(player.socketId).emit('points dashboard', { chatroomOwner: player.chatroomOwner, lastRound: lastRound, players: CHATROOMS[socket.room].players.map( p => { return { colorName: p.color, color: COLORS[p.color], points: p.points }; }) });
            });
                  
            return;
      }

      // Broadcast each players points to all other players.
   
      CHATROOMS[socket.room].players.forEach(function(player) {

            IO.to(player.socketId).emit('callback player points', { socketId: player.socketId , points: player.points });
            IO.to(player.socketId).emit('points dashboard', { chatroomOwner: player.chatroomOwner, lastRound: lastRound, players: CHATROOMS[socket.room].players.map( p => { return { colorName: p.color, color: COLORS[p.color], points: p.currentPoints }; }) });      
                  
            // Broadcast player points for all the players in the chatroom to see.

            let playerSocket =  SOCKET_CONNECTIONS.find(s => s.id === player.socketId);

            playerSocket.broadcast.to(socket.room).emit('broadcast player points', { socketId: player.socketId , points: player.points });
      });
}


/* This function starts a round. */ 

function nextRound(socket, callback) {

      // Emit facilitating text to all players that a round is starting.

      IO.sockets.to(socket.room).emit('facilitator-round-started', { facilitator: FACILITATING.ROUND_STARTED });
      
      // Start the next round of the game session.
      
      startRound(ROUND[Object.keys(ROUND)[CHATROOMS[socket.room].currentRound + 1]], socket);

      callback();
}


/* This function disconnects the player from both the CHATROOMS and SOCKET_CONNECTIONS. */

function disconnectPlayer(socket) {

      // Check whether the disconnected player is part of a chatroom.  

      if (typeof socket.room === 'undefined') {

            // The player is not part of any chatroom.

            // Find the specific socket that represents the connection of the player on the knock-knock_server, and than remove that socket.

            let playerSocket = SOCKET_CONNECTIONS.find(s => s.id === socket.id); SOCKET_CONNECTIONS.splice(SOCKET_CONNECTIONS.indexOf(playerSocket), 1);
            
            console.log('Socket with ' + socket.id + ' disconnected from knock-knock_server!');
            console.log('Number of sockets currently connected on the knock-knock_server: ' + SOCKET_CONNECTIONS.length);

            return;
      }

      // Find the player that has been disconnected with the specific socketId. 

      let disconnectedPlayer = CHATROOMS[socket.room].players.find(p => p.socketId === socket.id);

      // Check if there are at least three players in the chatroom.

      if(CHATROOMS[socket.room].gameStarted
            && CHATROOMS[socket.room].players.filter(p => p.socketId != disconnectedPlayer.socketId).length < MIN_NUMBER_OF_PLAYERS_READY
            && !disconnectedPlayer.chatroomOwner 
            && !CHATROOMS[socket.room].players.every(p => !p.chatroomOwner)) {

            // Find the chatroom owner.

            let chatroomOwner = CHATROOMS[socket.room].players.find(p => p.chatroomOwner === true);

            // Remove the chatroom owner.

            IO.to(chatroomOwner.socketId).emit('remove player from chatroom');
      }

      // Check whether the disconnected player is the chatroom owner.

      if(disconnectedPlayer.chatroomOwner) {

            // Find the specific socket that represents the connection of the disconnected player on the knock-knock_server, and than remove that socket.

            let playerSocket = SOCKET_CONNECTIONS.find(s => s.id === disconnectedPlayer.socketId); SOCKET_CONNECTIONS.splice(SOCKET_CONNECTIONS.indexOf(playerSocket), 1);

            console.log('Socket with ' + socket.id + ' disconnected from knock-knock_server!');
            console.log('Number of sockets currently connected on the knock-knock_server: ' + SOCKET_CONNECTIONS.length);

            // Remove disconnected player from the chatroom.

            CHATROOMS[socket.room].players.splice(CHATROOMS[socket.room].players.indexOf(disconnectedPlayer), 1);

            // Emit a socket event to player to leave the chatroom. This will redirect the player to the pre-chatroom-join.html page.

            IO.to(disconnectedPlayer.socketId).emit('remove player from chatroom');

            if(CHATROOMS[socket.room].players.length != 0) {

                  // If the disconnected player is the owner of the chatroom, then disconnect every other player in that chatroom. 

                  CHATROOMS[socket.room].players.forEach(function(player) {

                        IO.to(player.socketId).emit('remove player from chatroom');
                  });
            }
            else {
                  
                  console.log('Number of chatroom before deletion: ' + Object.keys(CHATROOMS).length);

                  delete CHATROOMS[socket.room];

                  console.log('Number of chatroom after deletion: ' + Object.keys(CHATROOMS).length);
            }
      }
      else {

            // Find the specific socket that represents the connection of the disconnectedPlayer on the knock-knock_server, and than remove that socket.

            let playerSocket =  SOCKET_CONNECTIONS.find(s => s.id === disconnectedPlayer.socketId);  SOCKET_CONNECTIONS.splice(SOCKET_CONNECTIONS.indexOf(playerSocket), 1);

            console.log('Socket with ' + socket.id + ' disconnected from knock-knock_server!');
            console.log('Number of sockets currently connected on the knock-knock_server: ' + SOCKET_CONNECTIONS.length);
            
            // Remove disconnected player from the chatroom.

            CHATROOMS[socket.room].players.splice(CHATROOMS[socket.room].players.indexOf(disconnectedPlayer), 1);
            CHATROOMS[socket.room].colors.splice(CHATROOMS[socket.room].colors.indexOf(disconnectedPlayer.color), 1);

            if(CHATROOMS[socket.room].gameStarted) {

                  CHATROOMS[socket.room].characters.splice(CHATROOMS[socket.room].characters.indexOf(disconnectedPlayer.currentCharacter.Key), 1);
                  CHATROOMS[socket.room].characterPhotographs.splice(CHATROOMS[socket.room].characterPhotographs.indexOf(disconnectedPlayer.currentCharacter.URL), 1);
            }            

            if(CHATROOMS[socket.room].players.length != 0) {

                   // Broadcast awareness that a disconnected player has left the chatroom.

                  IO.sockets.to(socket.room).emit('remove player from chatroom feedback', disconnectedPlayer.socketId);

                  if(CHATROOMS[socket.room].gameStarted) {

                        // If the disconnected player is the last person to make a guess before the points dashboard, broadcast the points.

                        if(CHATROOMS[socket.room].guessingSession 
                              && CHATROOMS[socket.room].players.filter(p => p.socketId != disconnectedPlayer.socketId).every( p => p.guessedWhoIsWho === true )) {

                              sendPlayerPoints(socket, false);
                        }

                        // If the disconnected players turn was to ask a question then start another round without him.

                        if(CHATROOMS[socket.room].turnToAskQuestion === disconnectedPlayer.color && !CHATROOMS[socket.room].guessingSession) {

                              // Emit facilitating text to all players that a round is starting.

                              IO.sockets.to(socket.room).emit('facilitator-round-started', { facilitator: FACILITATING.ROUND_STARTED });
      
                              // Start the same round of the game session.

                              startRound(CHATROOMS[socket.room].currentRound, socket);
                        }
                  }
            }
            else {

                  console.log('Number of chatroom before deletion: ' + Object.keys(CHATROOMS).length);

                  delete CHATROOMS[socket.room];

                  console.log('Number of chatroom after deletion: ' + Object.keys(CHATROOMS).length);
            }
      }
}


/* This function shuffles an array of items based on the Fisher–Yates shuffle algorithm */

function fisherYatesShuffle(array) {

      let currentIndex = array.length, temporaryValue, randomIndex;

      while (0 !== currentIndex) {
      
            randomIndex = Math.floor(Math.random() * currentIndex); currentIndex -= 1;

            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;     
      }

      return array;
}